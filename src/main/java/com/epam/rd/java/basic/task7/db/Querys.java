package com.epam.rd.java.basic.task7.db;

public class Querys {
    public static final String getAllUsers = "SELECT * FROM users";

    public static final String getAllTeams = "SELECT * FROM teams";

    public static final String insertUser = "INSERT INTO users (login) VALUES (?)";

    public static final String insertTeam = "INSERT INTO teams (name) VALUES (?)";

    public static final String setTeamsForUser = "INSERT INTO users_teams VALUES (? , ?)";

    public static final String getUser = "SELECT * FROM users WHERE login = (?)";

    public static final String getTeam = "SELECT * FROM teams WHERE name = (?)";

    public static final String getUserTeam = "SELECT id, name FROM teams JOIN users_teams\n" +
            "ON teams.id = users_teams.team_id\n" +
            "WHERE  users_teams.user_id = (?)";

    public static final String deleteUserTeam = "DELETE FROM users_teams WHERE team_id = (?)";

    public static final String deleteTeam = "DELETE FROM teams WHERE id = (?)";

    public static final String deleteTeamUser = "DELETE FROM users_teams WHERE user_id = (?)";

    public static final String deleteUser = "DELETE FROM users WHERE id = (?)";

    public static final String updateTeamName = "UPDATE teams SET name = (?) WHERE id = (?)";

}
