package com.epam.rd.java.basic.task7.db;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.*;
import java.util.*;

import com.epam.rd.java.basic.task7.db.entity.*;



public class DBManager {


    private static DBManager instance;
    private static Connection conn;

    private String getConnectionUrl() {
        Properties properties = new Properties();

        try (InputStream inputStream = Files.newInputStream(Paths.get("app.properties"))) {
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return properties.getProperty("connection.url"); // connection.url.my.sql
    }

    public static synchronized DBManager getInstance() {

        if (instance == null) {
            try {
                instance = new DBManager();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return instance;
    }


    private DBManager() throws SQLException {
        conn = DriverManager.getConnection(getConnectionUrl());
    }

    public List<User> findAllUsers() throws DBException {
        List<User> users = new ArrayList<>();

        try (Statement statement = conn.createStatement()) {

            ResultSet rs = statement.executeQuery(Querys.getAllUsers);

            while (rs.next()) {
                User user = new User();

                user.setId(rs.getInt("id"));
                user.setLogin(rs.getString("login"));

                users.add(user);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return users;
    }

    public boolean insertUser(User user) throws DBException {

        try (PreparedStatement statement = conn.prepareStatement(Querys.insertUser)) {

            statement.setString(1, user.getLogin());
            statement.execute();

            user.setId(getUser(user.getLogin()).getId());

            return true;
        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
    }

    public boolean deleteUsers(User... users) throws DBException {
        try (PreparedStatement statementUser = conn.prepareStatement(Querys.deleteUser)) {

            for (User user : users) {
                statementUser.setInt(1,user.getId());
                statementUser.execute();
            }

            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public User getUser(String login) throws DBException {
        try (PreparedStatement statement = conn.prepareStatement(Querys.getUser)) {

            statement.setString(1, login);
            ResultSet rs = statement.executeQuery();

            while (rs.next()) {
                User user = new User();

                user.setId(rs.getInt("id"));
                user.setLogin(rs.getString("login"));

                return user;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Team getTeam(String name) throws DBException {
        try (PreparedStatement statement = conn.prepareStatement(Querys.getTeam)) {

            statement.setString(1, name);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                Team team = new Team();

                team.setId(rs.getInt("id"));
                team.setName(rs.getString("name"));

                return team;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> list = new ArrayList<>();

        try (Statement statement = conn.createStatement()) {

            ResultSet rs = statement.executeQuery(Querys.getAllTeams);

            while (rs.next()) {
                Team team = new Team();

                team.setId(rs.getInt("id"));
                team.setName(rs.getString("name"));

                list.add(team);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return list;
    }

    //ok
    public boolean insertTeam(Team team) throws DBException {

        try (PreparedStatement statement = conn.prepareStatement(Querys.insertTeam)) {

            statement.setString(1, team.getName());
            statement.execute();

            team.setId(getTeam(team.getName()).getId());

            return true;
        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e);
        }
    }

    //ok
    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        //INSERT INTO users_teams VALUES (? , ?)
        try (PreparedStatement statement = conn.prepareStatement(Querys.setTeamsForUser)) {

            conn.setAutoCommit(false);

            for (Team team : teams) {
                statement.setInt(1, user.getId());
                statement.setInt(2, team.getId());
                statement.executeUpdate();
            }
            conn.commit();
            conn.setAutoCommit(true);

            return true;
        } catch (SQLException e) {
            try {
                conn.rollback();
            } catch (SQLException exception) {
                exception.printStackTrace();
            }
            throw new DBException("transaction has been failed", new SQLException());

        }
    }

    //ok
    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> list = new ArrayList<>();

        try (PreparedStatement statement = conn.prepareStatement(Querys.getUserTeam)) {

            user.setId(getUser(user.getLogin()).getId());
            statement.setString(1, String.valueOf(user.getId()));
            ResultSet rs = statement.executeQuery();

            while (rs.next()) {
                Team team = new Team();

                team.setId(rs.getInt("id"));
                team.setName(rs.getString("name"));

                list.add(team);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return list;
    }

    //ok
    public boolean deleteTeam(Team team) throws DBException {
        try (PreparedStatement statementTeams = conn.prepareStatement(Querys.deleteTeam)) {

            statementTeams.setInt(1, team.getId());
            statementTeams.execute();

            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    //ok
    public boolean updateTeam(Team team) throws DBException {
        try (PreparedStatement statement = conn.prepareStatement(Querys.updateTeamName)) {

            statement.setString(1, team.getName());
            statement.setInt(2, team.getId());
            statement.execute();

            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

}
